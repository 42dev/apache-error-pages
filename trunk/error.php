<?php
################################################
#                                              #
#  DAE - Dynamic Apache Errors                 #
#  Version 1.1 - 2005-03-30 Florian Beer       #
#  Version 2.0 - 2012-02-17 Florian Beer       #
#                                              #
#  Dieses Script steht unter der MDWDW Lizenz. #
#  Die 'Mach doch was du willst' Lizenz.       #
#  Viel Spass damit!                           #
#                                              #
################################################


class Error {
	var $error;
	var $msg;
	var $error_notes;
	var $img;
	var $host;
	var $admin;

	function __construct(){

		# Error Messages

		$this->msg['401']['sts'] = "Unauthorized"; 
		$this->msg['401']['txt'] = "You didn't say the magic word!";
		
		$this->msg['402']['sts'] = "Payment Required";
		$this->msg['402']['txt'] = "Won't get that for free!";
		
		$this->msg['403']['sts'] = "Forbidden";
		$this->msg['403']['txt'] = "You shall not pass!";
		
		$this->msg['404']['sts'] = "Not Found"; 
		$this->msg['404']['txt'] = "Something's missing.";
		
		$this->msg['405']['sts'] = "Method Not Allowed";
		$this->msg['405']['txt'] = "No, not like that!";
		
		$this->msg['406']['sts'] = "Not Acceptable";
		$this->msg['406']['txt'] = "What are you doing?";
		
		$this->msg['407']['sts'] = "Proxy Authentication Required";
		$this->msg['407']['txt'] = "No, you first have to ask my pal over there!";
		
		$this->msg['409']['sts'] = "Conflict";
		$this->msg['409']['txt'] = "What's up?";
		
		$this->msg['410']['sts'] = "Gone";
		$this->msg['410']['txt'] = "...silence...";
		
		$this->msg['411']['sts'] = "Length Required";
		$this->msg['411']['txt'] = "C'mon, show me!";
		
		$this->msg['414']['sts'] = "Request URI too large";
		$this->msg['414']['txt'] = "Uh oh, that was too long!";
		
		$this->msg['415']['sts'] = "Unsupported Media Type";
		$this->msg['415']['txt'] = "I don't know, what you're talking about!";

		$this->msg['417']['sts'] = "Expectation failed";
		$this->msg['417']['txt'] = "Request can not be satisfied.";

		$this->msg['src']['sts'] = "Direct Access";
		$this->msg['src']['txt'] = "Want to see the <a href=\"error.phps\">source</a>?";

		$this->msg['na']['sts'] = "Not Available";
		$this->msg['na']['txt'] = "Error Code is not supported by this Script or Apache.<br>Want to see the <a href=\"error.phps\">source</a>?";

		# Which Error Code?
		if(isset($_SERVER['REDIRECT_STATUS'])){
			$this->error = $_SERVER['REDIRECT_STATUS'];
		} else {
			$this->error = isset($_GET['e'])?intval($_GET['e']):'src';
		}
		
		# Invalid Error Code?
		$this->error = isset($this->msg[$this->error]) ? $this->error : 'na';

		# Maybe we even have a Server Message?
		$this->error_notes = isset($_SERVER['REDIRECT_ERROR_NOTES'])?$_SERVER['REDIRECT_ERROR_NOTES']:null;

		# Check for Picture
		if(file_exists('errors/'.$this->error.'.png')){
        	$this->img = '<img src="'.$this->error.'.png" alt="'.$this->error.'">';
        }

		# Set Contact eMail	
		$this->admin = "XXX";
	}
}

$page = new Error();
?>
<!DOCTYPE html>
<html lang="en">
<meta charset=utf-8>
<title>Error <?php echo $page->error;?> | <?php echo $_SERVER['SERVER_NAME'];?></title>
<link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
<style>
*{margin:0;padding:0;}
body {margin:90px;font-family:"Play",sans-serif;color:#000;}
a{color:inherit;}
small{display:block;margin-bottom:100px;}
p,small{color:#999;}
</style>
<h1>Error <?php echo $page->error;?> - <?php echo $page->msg[$page->error]['sts'];?></h1>
<h2><?php echo $page->img;?> <?php echo $page->msg[$page->error]['txt'];?></h2>
<p><?php echo $page->error_notes;?></p>
<small>In case of questions, contact: <a href="mailto:<?php echo $page->admin;?>"><?php echo $page->admin;?></a><br>
&copy; <?php echo date('Y');?> <a href="http://<?php echo $_SERVER['SERVER_NAME'];?>"><?php echo $_SERVER['SERVER_NAME'];?></a></small>
<script><!--
google_ad_client = "XXX";
google_ad_slot = "XXX";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>